# encoding=utf-8
from __future__ import unicode_literals, print_function

import os, sys

PROJECT_DIR = '/var/www/fibaro_reader'

sys.path.insert(0, os.path.join(PROJECT_DIR, "src"))
sys.path.insert(0, PROJECT_DIR)


def execfile(filename):
    globals = dict( __file__ = filename )
    exec( open(filename).read(), globals )

activate_this = os.path.join( PROJECT_DIR, 'venv/bin', 'activate_this.py' )
execfile( activate_this )

from app import app as application

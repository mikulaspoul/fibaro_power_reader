# encoding=utf-8
from __future__ import unicode_literals, print_function

import csv

from unipath import Path

try:
    from config import config
except ModuleNotFoundError:
    from .config import config


def create_if_empty(file_path):
    file_path = Path(file_path)
    file_dir = file_path.parent

    if file_dir:
        file_dir.mkdir(parents=True)

    if not file_path.exists():
        with open(file_path, mode='w') as fl:
            writer = csv.writer(fl, delimiter=";")

            definition = ['Date']

            for i in range(len(config["read_ids"])):
                definition += ['Device {} Power'.format(i + 1), 'Device {} Energy'.format(i + 1)]

            writer.writerow(definition)

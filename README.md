Install

```
sudo apt-get install python3 python-virtualenv python3-virtualenv supervisor apache2 git libapache2-mod-wsgi-py3
```

Update supervisor.conf, include section

```
 /var/www/fibaro_reader/src/fibaro_reader.conf
```

Update WSGI

```

<VirtualHost *:80>
        ErrorLog /home/pi/data/fibaro_data/logs/apache_error.log
        CustomLog /home/pi/data/fibaro_data/logs/apache_access.log combined

        Options -Indexes

        <Directory /var/www/fibaro_reader/src/>
                <Files wsgi.py>
                         Order allow,deny
                        Allow from all
                </Files>
        </Directory>

        DocumentRoot /var/www/fibaro_reader/src/
        WSGIDaemonProcess fibaro_reader processes=1 threads=2 display-name=%{GROUP}
        WSGIProcessGroup fibaro_reader
        WSGIApplicationGroup %{GROUP}
        WSGIScriptAlias / /var/www/fibaro_reader/src/wsgi.py
        WSGIScriptReloading On

</VirtualHost>

```

Create folder

```
/home/pi/data/fibaro_data/logs/
```

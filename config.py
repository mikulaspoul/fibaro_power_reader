# encoding=utf-8
from __future__ import unicode_literals, print_function

import yaml
from unipath import Path

with open(Path(Path(__file__).parent, "config.yaml"), 'r') as stream:
    try:
        config = yaml.load(stream)["config"]
    except yaml.YAMLError as exc:
        pass

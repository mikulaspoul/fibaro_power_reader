# encoding=utf-8
import csv
import time
from datetime import datetime

import requests
import json
from app import sentry
from config import config
from utils import create_if_empty


class ReaderError(Exception):
    pass


class Reader(object):

    def __init__(self, ip, user="admin", password="admin", ids={5, 8, 11, 14}):
        self.ip = ip
        self.user = user
        self.password = password
        self.ids = ids

    def uri(self, device_id):
        return "http://{}/api/devices/{}".format(self.ip, device_id)

    def read(self, device_id):
        try:
            response = requests.get(self.uri(device_id), auth=(self.user, self.password))

            if response.status_code == 200:

                data = json.loads(response.content.decode("utf-8"))

                try:
                    return [data["properties"]["power"], data["properties"]["energy"]]
                except KeyError:
                    raise ReaderError
            else:
                raise ReaderError
        except:
            sentry.captureException()
            raise ReaderError

    def read_row(self):
        try:
            res = [datetime.now().isoformat()]
            for i in self.ids:
                res += self.read(i)
            return res
        except ReaderError:
            return [
                datetime.now().isoformat(),
                "", "", "", ""
            ]
        except KeyboardInterrupt:
            print("")
        except:
            sentry.captureException()
            return [
                datetime.now().isoformat(),
                "", "", "", ""
            ]

if __name__ == "__main__":
    reader = Reader(config["fibaro_ip"], user=config["user"], password=config["password"], ids=config["read_ids"])

    create_if_empty(config["file_path"])

    try:
        while True:

            with open(config["file_path"], "a+") as fl:
                writer = csv.writer(fl, delimiter=";")
                writer.writerow(reader.read_row())

            time.sleep(5)
    except KeyboardInterrupt:
        pass

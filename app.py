# encoding=utf-8
import json

from pathlib import Path

from flask import Flask, render_template, request, Response
from raven.contrib.flask import Sentry

try:
    from config import config
    from utils import create_if_empty
except ModuleNotFoundError:
    from .config import config
    from .utils import create_if_empty


app = Flask(__name__)
sentry = Sentry(app, dsn=config.get("sentry_dsn"))


def tail(fl, n=0, offset=0):
    with fl.open("r") as f:
        lines = f.readlines()

    if n:
        return "".join(reversed(lines[-n:])), len(lines)
    else:
        return "".join(reversed(lines[offset:])), len(lines) - offset


@app.route('/', methods=['GET'])
def index():
    return render_template("index.html", gauges=config.get("gauges"), name=config.get("name", "Reader 2.0"))


@app.route('/raw', methods=['GET'])
def raw():
    try:
        create_if_empty(config["file_path"])

        fl = Path(config["file_path"])

        if not request.args.get("tail") and not request.args.get("offset"):
            with fl.open() as f:
                return "<div style='white-space: pre-line;'>{}</div>".format(f.read())

        if request.args.get("tail"):
            lines, length = tail(fl, int(request.args.get("tail")))
            data = {
                "total_length": length,
                "lines": lines
            }

            response = Response(json.dumps(data), content_type="application/json")

            return response

        if request.args.get("offset"):
            lines, length = tail(fl, offset=int(request.args.get("offset")))
            data = {
                "length": length,
                "lines": lines
            }

            response = Response(json.dumps(data), content_type="application/json")

            return response

    except Exception as e:
        if app.config["DEBUG"]:
            raise
        sentry.captureException()
        return "Server Error", 500


@app.route('/gauges', methods=['GET'])
def gauges():
    try:
        create_if_empty(config["file_path"])

        fl = Path(config["file_path"])
        line, _ = tail(fl, 1)
        line = line.split(";")

        data = {}

        for gauge_id, gauge in config["gauges"].items():
            if config.get("gauges_mode", "fibaro") == "fibaro":

                idx = 1 + config["read_ids"].index(gauge["id"]) * 2

                if gauge["mode"] == "energy":
                    idx += 1

                try:
                    value = float(line[idx])
                except (IndexError, ValueError):
                    value = 0
            else:
                idx = gauge["index"]

                try:
                    value = float(line[idx])
                except (IndexError, ValueError):
                    value = 0

            if gauge.get("multiplier"):
                value = value * float(gauge.get("multiplier"))

            data[gauge_id] = {
                "value": round(value, 2)
            }

        return Response(json.dumps(data), content_type="application/json")

    except Exception as e:
        if app.config["DEBUG"]:
            raise
        sentry.captureException()
        return "Server Error", 500


if __name__ == '__main__':
    app.run()
